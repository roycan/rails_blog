require 'test_helper'

class PagesTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test "should get home" do 
    get welcome_index_url
    assert_response :success
  end

  test "should get root" do
    get root_url
    assert_response :success
  end

  test "should get articles" do
    get articles_url 
    assert_response :success
  end 

  test "should get new_article" do
    get new_article_url
    assert_response :success
  end 

  test "should get edit_artcle" do
    get edit_article_url
    assert_response :success
  end
end
